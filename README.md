Comunidades Paraenses de TI e Inovação
======================================

As comunidades paraenses de tecnologia e afins estão bombando na vida real e nos grupos no Telegram e WhatsApp! Participe você também. Esse repositório contém uma lista organizada das comunidades (e seus meios de comunicação) por temas na tecnologia.

Inspiração no [Repositório de Canais e Grupos Brasileiros de TI ](https://github.com/alexmoreno/telegram-br) e no [Repositório de Comunidades Potiguares de TI](https://gitlab.com/pbaesse/comunidades-ti-rn). Conheças várias outras comunidades lá também.

Para realizar pedidos de novas comunidades:
1. Crie um pull request para adicionar novas comunidades;
2. Retire o :triangular_flag_on_post: das comunidades com esse ícone e coloque nas novas comunidades adicionadas;
3. Avise no grupo do [ParaLivre no Telegram](http://t.me/paralivre) a alteração.
**OBS**: Caso não saiba usar o GitLab, basta pedir para que sua comunidade seja adicionada aqui no grupo do telegram do [ParaLivre](http://t.me/paralivre). 

# Eventos
- **Agenda TI Pará**: Agenda de Eventos de TI no Pará
  - [Site](https://www.agendatipara.com.br)
  - [Calendário](https://www.agendatipara.com.br/calendario)  
  - [Telegram](https://t.me/agendatipara)  
  - [X](https://x.com/agendatipara)  
  - [Facebook](https://www.facebook.com/agendatipara)
  - [Instagram](https://www.instagram.com/agendatipara) 
  - [Linkedin](https://www.linkedin.com/company/agendatipara/)  
  - [WhatsApp Canal](https://whatsapp.com/channel/0029VaTNlWtBFLgZbekaiT34)    

# Software Livre e Open Source
- **ParaLivre**: Comunidade Paraense de Software Livre
  - [Site](https://www.paralivre.org)
  - [Telegram](https://t.me/paralivre)  
  - [X](https://x.com/paralivre_)
  - [Instagram](https://www.instagram.com/paralivre)
  - [Youtube](https://www.youtube.com/channel/UC4NEBkVLVLISm231UXbqjGg)    

- **LabLivre**: Laboratório Experimental de Tecnologias Livre
  - [Site](https://lablivre.tec.br/)
  - [Instagram](https://www.instagram.com/lablivreamazonia/)
  - [Youtube](https://www.youtube.com/channel/UCgmRG_LNCgOrZTctuwJbygA)  
  
- **Centro de Competência em Software Livre - UFPA**
  - [Site](http://ccsl.ufpa.br)
 
# Linguagens de Programação
- **GruPy Belém**: Comunidade de Python de Belém
  - [Telegram](https://t.me/GruPy_Belem)
  - [WhatsApp](https://chat.whatsapp.com/ERHL4P2YfUqHIFlO9uorMr)
  
- **PHP Pará**: Elephants Pará
  - [Telegram](https://t.me/PHPPA)
  - [X](https://x.com/phppara)
  - [Facebook](https://www.facebook.com/elephants.para)
  
- **Java Belém**: Galera do Java Belém PA
  - [WhatsApp](https://chat.whatsapp.com/FwzwJy9Lyx36iOaOQ4YhjA)
  - [Telegram](https://t.me/joinchat/LwNStBerdCoTZZIlrDjIPA)

- **Linguagem Egua**: Comunidade da Linguagem de Programação Egua
  - [Site](https://egua.dev/)
  - [GitHub](https://github.com/eguatech)
  - [Linkedin](https://www.linkedin.com/company/eguatech)    
  - [Instagram](https://www.instagram.com/eguatech)
  - [Youtube](https://www.youtube.com/channel/UCDgGUdR_6hZ6lfVaQbkQPLw)

- **Elug-PA**: Comunidade Elixir Pará :triangular_flag_on_post:
  - [Telegram](https://t.me/elug_pa)
 
# Programação e Desenvolvimento Aplicado e Ágil
- **Devs Norte**: Comunidade de desenvolvedores do Norte do Brasil
  - [GitHub](https://www.github.com/devsnorte/)
  - [Telegram](https://t.me/devsnorte)
  - [WhatsApp - Canal](https://whatsapp.com/channel/0029Va4Qpqs6mYPW90VYNQ2p)
  - [Facebook](https://www.facebook.com/devsnorte)
  - [Instagram](https://www.instagram.com/devsnorte)
  - [X](https://x.com/devsnorte)
  - [Youtube](https://www.youtube.com/channel/UCYyYXAdZisrAScOlG6w9KHQ)
  - [Linkedin](https://www.linkedin.com/company/devsnorte/)
  - [Discord](https://discord.gg/xeFx2HPYSq)
  - [DEV Community Profile](https://dev.to/devsnorte)  
  
- **Tá Safo**: Tecnologias Abertas com Software Ágil, Fácil e Organizado
  - [Slack](https://tasafo.slack.com)
  - [X](https://x.com/tasafo)
  - [Facebook](https://www.facebook.com/tasafo.comunidade)
  - [Instagram](https://www.instagram.com/comunidadetasafo)
  - [Youtube](https://www.youtube.com/channel/UCk7I56rIl0qBeAVtiT-3ruQ)  

- **Joomla Belém**: Grupo de Usuários Joomla! Belém 
  - [Listagem User Group](https://community.joomla.org/user-groups/joomla-user-group-belem.html)
  - [Site](https://joomlabelem.paralivre.org)
  - [Telegram](https://t.me/joomlabelem)
  - [Instagram](https://www.instagram.com/joomlabelem)
  - [X](https://x.com/joomlabelem)        
  - [Facebook](https://www.facebook.com/joomlabelem)  
  - [Linkedin](https://www.linkedin.com/company/joomlabelem/)  
  - [Youtube](https://www.youtube.com/channel/UCD9TFrfankxIlJdjrCu-JVA)    

- **Flutter Pará**: Grupo de Comunidade Flutter 
  - [WhatsApp](https://chat.whatsapp.com/LBJrF2x54gU4DxYYzMhgWt)
   
# Desenvolvimento de Jogos
- **Bel Jogos**: Grupo de entusiastas, hobbistas, apreciadores e desenvolvedores de jogos eletrônicos em Belém
  - [Site](http://www.beljogos.com.br/)
  - [Instagram](https://www.instagram.com/beljogos)
  - [X](https://x.com/beljogos)
  - [Facebook](https://www.facebook.com/BeljogosPA)
  - [Youtube](https://www.youtube.com/channel/UCa0FvmBQz0JiActsGs5r5-A)  
  - [Flickr](https://www.flickr.com/photos/beljogos/) 
  - [Linkedin](https://www.linkedin.com/groups/1480417/)
  - [Discord](https://discord.gg/25DtKFsVMy)  
  
- **GameDevsPA**: Grupo multidisciplinar formado por diversos profissionais que trabalham em conjunto para o fortalecimento da indústria de jogos no estado do Pará.
  - [Instagram](https://www.instagram.com/gamedevspa)
  - [Facebook Fan Page](http://facebook.com/gamedevspa)
  - [Facebook Grupo](https://www.facebook.com/groups/117244738954945/)  
  - [Youtube](https://www.youtube.com/channel/UCOzB-hyIPS_DanMritkuHKA)  
  
# DEVOPS
- **DevOps Parauapebas**: Grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas
  - [Telegram](https://t.me/joinchat/A-G57xd_fjAnQwOzV_sdeQ)
  - [Instagram](https://instagram.com/devopspbs)
  - [GitLab](https://gitlab.com/devopspbs)
  - [Site](https://devopspbs.org)    

# Maker
- **Arduino Parauapebas**: Grupo que promove eventos, projetos, grupo de estudo e todas as outras possibilidades envolvendo o Arduino e outros embarcados
  - [Youtube](https://www.youtube.com/channel/UCDSOUrnRptHkP9Atv-zO_qg)
  - [Facebook](http://www.facebook.com/ArduinoParauapebas)

# Ciência de Dados, Banco de Dados, Data Platform
- **Açaí com Dados**: Grupo com discussão acerca da área de dados com o objetivo de fomentar essa área no Estado.
  - [WhatsApp](https://chat.whatsapp.com/DvhIxBoNoGUFiLRK1emZrL) 
  - [GitHub](https://github.com/acaicomdados)
  - [Instagram](https://instagram.com/acaicomdados) 
  - [Linkedin](https://www.linkedin.com/company/acai-com-dados/)

- **SQL Norte**: O grupo compartilha conhecimento sobre dados em eventos, promove o networking na Comunidade do Norte do País
  - [WhatsApp Grupo 1](https://chat.whatsapp.com/EzD5z7bpXc41fbW0TCw31Q)
  - [WhatsApp Grupo 2](https://chat.whatsapp.com/BCWb6O09jPA1VWZGgIygG7)  
  - [Telegram](https://t.me/joinchat/H3xPKxFX2iO8wKMyU55kiQ)
  - [Facebook](https://www.facebook.com/sqlnorte)
  - [Instagram](https://www.instagram.com/sqlnorte_oficial)
  - [X](https://x.com/sqlnorte)
  - [Youtube](https://www.youtube.com/channel/UCBvWwvJQY0JaoHC2YgJrJ2Q)
  - [Linkedin](https://www.linkedin.com/company/sqlnorte)
  - [Meetup](https://www.meetup.com/pt-BR/SQL-Norte)
     
# Promoção e reforço da participação feminina na tecnologia
- **ArduLadies Belém**: Somos uma comunidade de mulheres makers apaixonadas por eletrônica, tecnologias, robótica e programação. 
  - [GitHub](https://github.com/arduladies-belem)
  - [Facebook](https://www.facebook.com/Arduladies-Bel%C3%A9m-259851468268070/)
  - [Instagram](https://www.instagram.com/arduladiesbelem/)
  - [X](https://x.com/ArduladiesB)
  
- **DevOps Girls Parauapebas**
  - [WhatsApp](https://chat.whatsapp.com/L2L2emg0pCy5K1xoyP6IYE)
       
- **Django Girls Belém**
  - [Facebook](https://www.facebook.com/DjangoGirlsBelem)
  - [Site](https://djangogirls.org/belem)
 
- **IEEE WIE UFPA**: O Ramo Estudantil IEEE UFPA Women in Engineering tem como objetivo incentivar a participação feminina na área de engenharia. 
  - [Instagram](https://www.instagram.com/ieeewieufpa)
  
- **TacaCode Hub / Manas Digitais**: Tem como objetivo a realização de práticas de caráter motivacional e informativo para promover a carreira na computação e áreas tecnológicas
  - [Facebook](https://www.facebook.com/manasdigitais)
  - [Instagram](https://www.instagram.com/manasdigitais)
  - [Linkedin](https://www.linkedin.com/in/manas-digitais/)  
  - [Youtube](https://www.youtube.com/channel/UCOCH0xarZnhnuPs2hbBMnxQ)  
 
- **Meninas da Geotecnologia**: Projeto de Extensão do IFPA campus Castanhal, chancelado pela Sociedade Brasileira de Computação.
  - [Facebook](https://www.facebook.com/meninasdageo)
  - [Instagram](https://www.instagram.com/meninasdageo)
  - [Site](https://meninasdageo.com.br/)
  - [WhatsApp Comunidade](https://chat.whatsapp.com/HSrD82rNrKnI6Cxqp8mHxl)  
  - [WhatsApp Canal](https://chat.whatsapp.com/FOnuFv9Baz2A4uBaWAzBDQ)
  - [Youtube](https://www.youtube.com/channel/UCQISKuKO0IyaBsL4BhHnrFg)    
  - [Spotify](https://open.spotify.com/show/0lnKDEv9jX8tr9uyzB8NLv)
  
- **Meninas de Sistemas**: Fortalecendo a presença feminina na Área da Computação. Presente na UFPA nas cidades de Cametá, Oeiras do Pará e Limoeiro do Ajuru. 
  - [Instagram](https://www.instagram.com/meninasdesistemas)
  - [Site](https://meninas.sbc.org.br/projetos-parceiros/meninas-de-sistemas/)    

- **Meninas Paid’éguas**: Tem como objetivo despertar o interesse e fomentar a inclusão de meninas estudantes do ensino médio em carreiras na área de Computação e Ciências Exatas, nos municípios de Belém e Castanhal.  
  - [Site](https://meninaspaideguas.ufpa.br/)
  - [Instagram](https://www.instagram.com/meninaspaideguas/)
  - [Facebook](https://www.facebook.com/meninaspaideguas)
  - [Youtube](https://www.youtube.com/channel/UC7cRrYk3o0VP7HxMG2A7iag)      
 
- **NiaraTech - Tecnologia Social**: é um projeto social que tem o objetivo de tornar a área de tecnologia mais acessível às meninas e mulheres da periferia, dando oportunidade a inserção ao mercado de trabalho.  
  - [Instagram](https://www.instagram.com/niaratechtecnologia/)
  
- **Pérolas Digitais**: É um projeto de extensão da Universidade Federal do Oeste do Pará (UFOPA), de alunas do curso de Ciência da Computação e Sistemas de Informação com o propósito de incentivar e promover o ingresso de mais meninas na área da computação. 
  - [Instagram](https://www.instagram.com/perolas_digitais/)
  - [Site](https://meninas.sbc.org.br/projetos-parceiros/perolas-digitais/)
  
- **Programa Mulheres e Meninas nas Engenharias**: Grupo de mulheres acadêmicas de Engenharias da UFPA campus de Tucuruí/PA.  Estimula a participação de mais mulheres nas áreas de computação e exatas. 
  - [Instagram](https://www.instagram.com/pmme.camtuc/) 
  - [Facebook](https://www.facebook.com/profile.php?id=100042064487061) 
  - [Linkedin](https://www.linkedin.com/company/pmecamtuc/) 
  - [Youtube](https://www.youtube.com/channel/UCJPPyXMSU8Qlq7al4mIgBLg) 

- **PyLadies Belém**: É um grupo internacional, com foco em ajudar mais mulheres a se tornarem participantes ativas e líderes da comunidade de código aberto Python. 
  - [Facebook](https://www.facebook.com/pyladiesbelem)
  - [Instagram](https://www.instagram.com/pyladiesbelem)
    
- **PyManas**: É uma comunidade para motivação, aprendizado, desafios e dicas sobre Python.  
  - [Instagram](https://www.instagram.com/pymanas)
 
- **Rails Girls Belém**: É uma comunidade para que as mulheres entendam a tecnologia e construam suas idéias.
  - [Facebook](https://www.facebook.com/railsgirlsbelem)
  - [Instagram](https://www.instagram.com/railsgirlsbelem)
  - [Site](http://railsgirls.com/belem)  

- **Tech Manas**: Projeto Meninas na Computação da UFPA campus de Tucuruí/PA. Estimula a participação de mais mulheres nas áreas de computação e exatas.  
  - [Instagram](https://www.instagram.com/tech_manas)  
  - [Site](https://meninas.sbc.org.br/projetos-parceiros/techmanas/)  

# Segurança, Redes, Infraestrutura e Tecnologia em geral
- **AWS User Group Belém**: Comunidade belenense de usuários AWS.  
  - [Instagram](https://www.instagram.com/awsbelem)
  - [Meetup](https://www.meetup.com/awsbelem/)  
 
- **Forense Pai d’Égua!**  
  - [WhatsApp](https://chat.whatsapp.com/DhI1yqLvxXRFF4ZF3wLHLy)

- **OWASP Belém**  
  - [Instagram](https://www.instagram.com/owasp_belem/)
  - [Facebook](https://www.facebook.com/OwaspBelem21)
  - [Site](https://owasp.org/www-chapter-belem/) 
  - [Youtube](https://www.youtube.com/channel/UCikvPPNK2In_JDj7ClpK6ew)
  - [Meetup](https://www.meetup.com/pt-BR/owasp-belem/)  

- **XibéSec**: É Um ponto de encontro entre Profissionais do Mercado, Academia, Estudantes e Entusiastas da área de Tecnologia da Informação. O projeto foi criado pensando no desenvolvimento da nossa região e conta com a solidariedade de diversos profissionais na organização, tutoria ou simples participação e discussão dos mais variados tópicos. 
  - [Instagram](https://www.instagram.com/xibesec/)
  - [Discord](https://discord.gg/s6j3vnGjmE)

# Gerenciamento de Projetos
- **CGP - Comunidade de Gerenciamento de Projetos Pará**: É um grupo sem fins lucrativos que pretende ampliar as oportunidades de qualificação técnica da região na área, principalmente pela socialização de informações relevantes. 
  - [WhatsApp](https://chat.whatsapp.com/5lXO9dWLf9UAGgkLfroq5d)
  - [Instagram](https://www.instagram.com/cgppara/)
  - [Facebook](https://www.facebook.com/cgppara/)
  - [Youtube](https://www.youtube.com/channel/UCuUKvyUKHiD56ZWtettcvgA)
  - [Site](http://cgppara.blogspot.com/) 

# Capítulos locais de organizações internacionais
- **GDG Belém**: Somos o Google Developers Group oficial de Belém
  - [Site](https://gdg.community.dev/gdg-belem/)
  - [WhatsApp](https://chat.whatsapp.com/Kj1skddvixK1MPcCaKDx0n)  
  - [Instagram](https://www.instagram.com/gdgbelemoficial)
  - [X](https://x.com/GDGBelem)
  - [GitHub.io](https://gdgbelem.github.io/home/)
  - [Slack](https://gdgbelem.slack.com/)
  - [Youtube](https://www.youtube.com/channel/UCZhDFAppB-6mGRLfIbhLEOQ)
  - [Linkedin](https://www.linkedin.com/company/gdgbelem/)          
  
- **Legal Hackers Belém**: Primeiro capítulo paraense do movimento global Legal Hackers! Conectando o Direito, Tecnologia e Inovação.
  - [Instagram](https://www.instagram.com/legalhackersbelem)
  - [Facebook](https://www.facebook.com/belemlegalhackers)
  - [X](https://x.com/legalhackersbel)        

- **PMI Branch Pará**: É a ramificação do PMI (Project Management Institute) Amazônia Chapter no Estado do Pará.
  - [Instagram](https://www.instagram.com/pmiamoficial)
 
- **IEEE UFPA**: Ramo Estudantil da IEEE (Institute of Electrical and Electronics Engineers) na UFPA.
  - [Instagram](https://www.instagram.com/ieeeufpa)
  - [Facebook](https://www.facebook.com/ieeeufpa)
  - [Youtube](https://www.youtube.com/channel/UCKDrLqI2GlVUqm9U23bNN0Q)  

# Associações Empresariais, Empreendedorismo e Sindicato
- **Açaí Valley**: Associação Paraense de Tecnologia e Inovação
  - [Site](https://www.acaivalley.com.br/)
  - [Facebook](https://www.facebook.com/acaivalley)
  - [Instagram](https://www.instagram.com/acaivalley)
  - [Telegram](https://t.me/acaivalley)
  - [Slack](https://bit.ly/slackcomunidadeacaivalley)
  - [Youtube](https://www.youtube.com/channel/UCvISsZbnXWkq9XcZavY8sJg)     

- **ParaTic**: Associação das Empresas Paraenses de Software e Tecnologia da Informação e Comunicação
  - [Site](http://www.paratic.com.br)
  - [Facebook](https://www.facebook.com/paraticbrasil)  
  - [Instagram](https://www.instagram.com/associacao_paratic)
    
- **Bate Papo Paidegua**: É uma comunidade que foi fundada para fortalecer o ecossistema empreendedor paraense. 
  - [Site](https://www.batepapopaidegua.com.br/)
  - [Instagram](https://www.instagram.com/batepapopaidegua/)
    
- **SINDPD-PA**: Sindicato dos Trabalhadores e Trabalhadoras em Tecnologias da Informação no Estado do Pará. 
  - [Site](https://www.sindpdpa.org.br/)
  - [Instagram](https://www.instagram.com/redesindpdpa)
  - [Facebook](https://www.facebook.com/redesindpdpa)
  - [X](https://x.com/sindpdpa)
